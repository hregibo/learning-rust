fn vectors () {
    // Vectors (Vec)
    let mut v: Vec<i32> = Vec::new();
    let v_macro = vec![1,2,3];
    v.push(5);
    v.push(6);
    v.push(7); // we added 'mut' to 'let v' 
    v.push(8); // because we can't push if immutable

    let third: &i32 = &v[2];
    println!("third element is {}", third);

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("there is no third element"),
    }

    let mut v2 = vec![1,2,3,4,5];
    // let first = &v2[0];
    // we can't borrow immutable ref as we rewrite the vec
    // in another address, by pushing below a new value
    v2.push(6);
    // println!("the first element is {}", first);

    let v3 = vec![100, 32, 57];
    for i in &v3 {
        println!("{}", i);
    }

    let mut v4 = vec![100, 32, 57];
    for i in &mut v4 {
        *i += 50;
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}

fn strings () {
    let mut s01 = String::from("foo");
    let s02 = "bar";
    s01.push_str(s02);
    println!("s2 is {}", s02);

    let mut s03 = String::from("lo");
    s03.push('l');
    // -> lol

    let s04 = String::from("Hello, ");
    let s05 = String::from("world!");
    let s06 = s04 + &s05;

    let s07 = String::from("tic");
    let s08 = String::from("tac");
    let s09 = String::from("toe");

    // let s10 = s07 + "-" + &s08 + "-" + &s09;
    // ^ takes ownership, unable to use s07 later on
    let s11 = format!("{}-{}-{}", s07, s08, s09);
    // ^ does not take ownership, all will be still 
    // usable, + its more readable

    let s12 = String::from("hello");
    // let h = s12[0];
    // ^ triggers an error

    let hello = "Здравствуйте";
    let s13 = &hello[0..4];
    

    for c in "नमस्ते".chars() {
        println!("{}", c);
    }
    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }
}

use std::collections::HashMap;
fn hashmaps () {
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let mut scores2: HashMap<String, i32> = HashMap::new();

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];
    let mut scores02: HashMap<_, _> = teams.into_iter().zip(initial_scores.into_iter()).collect();

    let field_name =  String::from("Favourite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field name and field value are invalid at this poiint,
    // try using them and see what compiler error you get!

    let team_name = String::from("Blue");
    let score = scores.get(&team_name);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    let mut scores04 = HashMap::new();
    scores04.insert(String::from("Blue"), 10);
    scores04.insert(String::from("Blue"), 25);
    println!("{:?}", scores04);

    let mut scores05 = HashMap::new();
    scores05.insert(String::from("Blue"), 10);

    scores05.entry(String::from("Yellow")).or_insert(50);
    scores05.entry(String::from("Blue")).or_insert(50);
    println!("{:?}", scores05);

    let text = "Hello world wonderful world";
    let mut map = HashMap::new();
    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);
}
fn main() {
    vectors();
    strings();
    hashmaps();
}

enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}
