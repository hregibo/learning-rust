#![allow(dead_code)]
#![allow(unreachable_code)]
#![allow(overflowing_literals)]

use fmt::Display;
use std::{error, fmt, mem};
use std::convert::{TryFrom, TryInto};

use my_mod::{nested, public_function_in_crate};

fn hello_world() {
    println!("Hello, world!");
    println!("I'm a Rustacean!");
}

fn formatted_print() {
    // base
    println!("{} days", 31);
    // forced type
    println!("{} days", 31i64);
    // positional arguments
    println!("{0} this is {1}. {1}, this is {0}.", "Alice", "Bob");
    // called arguments
    println!("{subject} {verb} {object}",
        object="the lazy dog",
        subject="the quick brown fox",
        verb="jumps over"
    );
    // special formatting with :
    println!("{} of {:b} people know binary, the other half doesn't", 1, 2);
    // you can right align text with a given width (padding-left)
    println!("{number:>width$}", number=1, width=6);
    // fill padding with 0
    println!("{number:>0width$}", number=1, width=6);
    // rust checks for number of arguments
    println!("My name is {0}, {1} {0}.", "bond", "james");
    
    // cerate a strct
    #[allow(dead_code)]
    struct Structure(i32);
    // but it requires custom handling to be printed. be default {} wont print it
    // println!("this struct {} wont print". Structure(3));
    
    let pi = 3.141592;
    println!("Pi is roughly {:.3}", pi);
}

fn formatted_print_debug() {
    struct UnPrintable(i32); // by default anything outside std:: doesnt include Debug or Display
    #[derive(Debug)]
    struct DebugPrintable(i32); // can be printed as it inherints fmt::Debug
    // we can now use {:?} to print debug stuff

    #[derive(Debug)]
    struct Structure(i32);
    #[derive(Debug)]
    struct Deep(Structure);

    println!("{:?} months in a year.", 12);
    println!(
        "{1:?} {0:?} is the {actor:?} name.",
        "Slater", 
        "Christian", 
        actor="actor's"
    );
    // this is printable
    println!("Now {:?} will print!", Structure(3));
    // derive has one issue, we have no control on output looks
    println!("Now {:?} will print!", Deep(Structure(3)));

    #[derive(Debug)]
    struct Person<'a> {
        name: &'a str,
        age: u8
    }
    let name = "peter";
    let age = 27;
    let peter = Person { name, age };
    println!("{:#?}", peter);

    impl fmt::Display for Structure {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            // write strictkly the fiest element into the supplied output
            // stream: f. returnes result which indicates whether the
            // operation succeeded or failed. note that write! 
            // uses syntax which is very si,ilar to println!.
            write!(f, "{}", self.0)
        }
    }
}

fn formatted_print_display() {
    #[derive(Debug)]
    struct MinMax(i64, i64);
    impl fmt::Display for MinMax {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({}, {})", self.0, self.1)
        }
    }

    #[derive(Debug)]
    struct Point2D {
        x: f64,
        y: f64
    }
    impl fmt::Display for Point2D {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "x: {}, y: {}", self.x, self.y)
        }
    }

    let minmax = MinMax(0, 14);

    println!("Compare structures:");
    println!("Display: {}", minmax);
    println!("Debug: {:?}", minmax);

    let big_range = MinMax(-300, 300);
    let small_range = MinMax(-3, 3);

    println!(
        "The big range is {big} and the small is {small}", 
        small = small_range, 
        big=big_range
    );

    let point = Point2D { x: 3.3, y: 7.2 };
    println!("Display: {}", point);
    println!("Debug: {:?}", point);

    // Error: Both debug and display were implemented but {:b} requires fmt::Binary to be implemented. this will not work:
    // println!("Wgat does Point2D look like in binary: {:b}?", point);

    #[derive(Debug)]
    struct Complex {
        real: f64,
        imag: f64
    }
    impl fmt::Display for Complex {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{} + {}i", self.real, self.imag)
        }
    }
    let complex = Complex { real: 3.3, imag: 7.2 };
    println!("Complex structure:");
    println!("Display: {}", complex);
    println!("Debug: {:?}", complex);

    // implementing display for a vec
    struct List(Vec<i32>);
    impl fmt::Display for List {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            // extract the value using tuple indexing
            // and create a ref to vec
            let vec = &self.0;
            write!(f, "[")?;
            // iterate over v in vec while enumerating the iteration count in count
            for (count, v) in vec.iter().enumerate() {
                if count != 0 { write!(f, ", ")?; }
                write!(f, "{}: {}", count, v)?;
            }
            write!(f, "]")
        }
    }
    let v = List(vec![1,2,3]);
    println!("{}", v);
}

fn formatted_print_formatting() {
    struct City {
        name: &'static str,
        lat: f32,
        lon: f32,
    }
    impl fmt::Display for City {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let lat_c = if self.lat >= 0.0 { 'N' } else { 'S' };
            let lon_c = if self.lon >= 0.0 { 'E' } else { 'W' };
            write!(
                f, 
                "{}: {:.3}°{} {:.3}°{}",
                self.name,
                self.lat.abs(),
                lat_c,
                self.lon.abs(),
                lon_c
            )
        }
    }
    #[derive(Debug)]
    struct Color {
        red: u8,
        green: u8,
        blue: u8,
    }
    impl fmt::Display for Color {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "RGB ({r}, {g}, {b}) 0x{r:02X}{g:02X}{b:02X}", r=self.red, g=self.green, b=self.blue)
        }
    }

    for city in [
        City { name: "Dublin", lat: 53.3477789, lon: -6.259722 },
        City { name: "Oslo", lat: 59.95, lon: 10.75 },
        City { name: "Vancouver", lat: 49.25, lon: -123.1 },
    ].iter() {
        println!("{}", *city);
    }
    for color in [
        Color { red: 128, green: 255, blue: 90 },
        Color { red: 0, green: 3, blue: 254 },
        Color { red: 0, green: 0, blue: 0 },
    ].iter() {
        println!("{}", *color);
    }
}

fn primitives() {
    let logical: bool = true;
    let a_float: f64 = 1.0;
    let an_integer = 5i32;

    let default_float = 3.0;
    let default_integer = 7;

    let mut inferred_type = 12; // estimates as i32 but
    inferred_type = 4294967296i64; // this change will inferre it as i64

    let mut mutable = 12; // mutable i32
    mutable = 21;
    // mutable = true; // gives error, cant change type
    // but we can redeclare into a new type the same name
    // using shadowing
    let mutable = true;
}

fn primitives_tuples() {
    let pair = (1i32, true);
    let (integer, boolean) = pair;
    #[allow(unused_variables)]
    let reverted = (boolean, integer);

    #[derive(Debug)]
    struct Matrix(f32, f32, f32, f32);
    impl fmt::Display for Matrix {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "( {} {} )\n( {} {} )", self.0, self.1, self.2, self.3)
        }
    }


    let long_tuple = (1u8, 2u16, 3u32, 4u64, -1i8, -2i16, -3i32, -4i64, 0.1f32, 0.2f64, 'a', true);
    println!("long tuple first value: {}", long_tuple.0);
    println!("long tuple second value: {}", long_tuple.1);

    let tuple_of_tuples = ((1u8, 2u16, 2u32),(4u64, -1i8), -2i16);
    println!("tuple of tuples: {:?}", tuple_of_tuples);
    
    // But long Tuples cannot be printed
    // let too_long_tuple = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
    // println!("too long tuple: {:?}", too_long_tuple);
    // TODO ^ Uncomment the above 2 lines to see the compiler error

    let pair = (1, true);
    println!("pair is {:?}", pair);
    println!("reversed pair is {:?}", reverted);

    // for tuples with only one member, comma is mandatory
    // otherwise its a simple type with parenthesis around
    println!("one element tuple: {:?}", (1u32,));
    println!("simple integer: {:?}", (3u32));

    // tuple can be destructured to create bindings
    let tuple = (1, "hello", 4.5, true);
    let (a, b, c, d) = tuple;
    println!("{:?}, {:?}, {:?}, {:?}", a, b, c, d);

    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    
    fn transpose(orig: Matrix) -> Matrix {
        Matrix(orig.0, orig.2, orig.1, orig.3)
    }

    println!("Matrix:\n{}", matrix);
    println!("Transpose:\n{}", transpose(matrix));
}

fn primitives_arrays_and_slices() {
    fn analyze_slice(slice: &[i32]) {
        println!("first element of the slice: {}", slice[0]);
        println!("the slice has {} elements", slice.len());
    }
    let xs: [i32; 5] = [1, 2, 3, 4, 5];
    let ys: [i32; 500] = [0; 500];

    println!("first element of the array: {}", xs[0]);
    println!("second element of the array: {}", xs[1]);

    println!("number of elements in array: {}", xs.len());
    println!("array occupies {} bytes", mem::size_of_val(&xs));

    println!("borrow the whole array as a slice");
    analyze_slice(&xs);

    println!("borrow a section of the array as a slice");
    analyze_slice(&ys[1..4]);

    // out of bound indexing causes compile error
    // println!("{}", xs[5]);
}

fn custom_types_structures() {
    #[derive(Debug)]
    struct Person {
        name: String,
        age: u8,
    }
    // unit struct
    struct Unit;
    // tuple struct 
    struct Pair(i32, f32);

    #[derive(Debug)]
    // a struct with two fields
    struct Point {
        x: f32,
        y: f32,
    }
    
    #[derive(Debug)]
    struct Rectangle {
        top_left: Point,
        bottom_right: Point,
    }

    let name = String::from("Peter");
    let age = 27;
    let peter = Person { name, age };

    println!("{:?}", peter);

    let point: Point = Point { x: 10.3, y: 0.4 };
    println!("point coordinates: ({}, {})", point.x, point.y);
    let bottom_right = Point { x: 5.2, ..point };
    println!("second point: {}, {})", bottom_right.x, bottom_right.y);

    // destructure the point  using a let binding
    let Point {x: top_edge, y: left_edge } = point;
    let rectangle = Rectangle {
        top_left: Point { x: left_edge, y: top_edge },
        bottom_right: bottom_right,
    };

    // instanciate a unit struct
    let _unit = Unit;
    // instanciate a tuple struct
    let pair = Pair(1, 0.1);
    // access values of tuple struct
    println!("pair contains {:?} and {:?}", pair.0, pair.1);
    // destructure a tuple struct
    let Pair(integer, decimal) = pair;
    println!("pair contains {:?} and {:?}", integer, decimal);

    fn rect_area(Rectangle{ top_left, bottom_right }: &Rectangle) -> f32 {
        let width = bottom_right.x - top_left.x;
        let height = top_left.y - bottom_right.y;
        width * height
    }
    println!("rectangle {:?} has area of {}", rectangle, rect_area(&rectangle));
    fn square(coords: &Point, len: f32) -> Rectangle {
        Rectangle { 
            top_left: Point { ..*coords }, 
            bottom_right: Point { x: coords.x + len, y: coords.y + len }
        }
    }
    let point = Point { x: 10.0, y: 10.0 };
    let square = square(&point, 5.0);
    println!("here comes the square! {:?}", square);
}

fn custom_types_enums() {
    enum WebEvent {
        // cam be unit like
        PageLoad,
        PageUnload,
        // can be tuple struct
        KeyPress(char),
        Paste(String),
        // or c-like structures
        Click { x: i64, y: i64 },
    }

    // function which takes a webevent enum as an argument and returns nothing
    fn inspect(event: WebEvent) {
        match event {
            WebEvent::PageLoad => println!("page loaded"),
            WebEvent::PageUnload => println!("page unloaded"),
            // destructure c from inside the enum
            WebEvent::KeyPress(c) => println!("pressed '{}'.", c),
            WebEvent::Paste(s) => println!("pasted \"{}\".", s),
            // destructure click into x and y
            WebEvent::Click {x, y} => {
                println!("clicked at x={}, y={}.", x, y);
            },
        }
    }

    let pressed = WebEvent::KeyPress('x');
    // to_owned() creates an owned string from a string slice
    let pasted = WebEvent::Paste("my text".to_owned());
    let click = WebEvent::Click { x: 20, y: 80 };
    let load = WebEvent::PageLoad;
    let unload = WebEvent::PageUnload;

    inspect(pressed);
    inspect(pasted);
    inspect(click);
    inspect(load);
    inspect(unload);

    enum VeryVerboseEnumOfThingsToDoWithNumbers {
        Add,
        Substract,
    }
    // create a type alias
    type Operations = VeryVerboseEnumOfThingsToDoWithNumbers;
    let _x = Operations::Add;

    impl VeryVerboseEnumOfThingsToDoWithNumbers {
        fn run(&self, x: i32, y: i32) -> i32 {
            match self {
                Self::Add => x + y,
                Self::Substract => x - y,
            }
        }
    }
}

fn custom_types_enums_use() {
    enum Status {
        Rich,
        Poor,
    }
    enum Work {
        Civilian,
        Soldier,
    }

    // explicitly use each name so they are available without manual scoping
    use Status::{Poor, Rich};
    use Work::*;
    // use crate:: in front if the enum is not in the function itself but at root of file

    let status = Poor;
    let work = Civilian;

    match status {
        Rich => println!("the rich have lots of money!"),
        Poor => println!("the poor have no money..."),
    }
    match work {
        Civilian => println!("Civilians work!"),
        Soldier => println!("Soldiers fight!"),
    }
}

fn custom_types_enums_clike() {
    enum Number {
        Zero,
        One,
        Two,
    }

    enum Color {
        Red = 0xff0000,
        Green = 0x00ff00,
        Blue = 0x0000ff,
    }

    println!("zero is {}", Number::Zero as i32);
    println!("one is {}", Number::One as i32);

    println!("roses are {:06x}", Color::Red as i32);
    println!("violets are {:06x}", Color::Blue as i32);
}

fn custom_types_enums_testcase_linkedlist() {
    use List::*;
    // remember to use crate:: before if the enum is not inside the function!!!1
    enum List {
        // Cons: tuple struct that wraps an element and a pointer to the next node
        Cons(u32, Box<List>),
        // Nil: A node that signifies the end of the linked list
        Nil,
    }
    impl List {
        fn new() -> List {
            Nil // Nil has type List
        }
        // no & at self means we get ownership = will be "eaten" by the fn
        fn prepend(self, elem: u32) -> List {
            Cons(elem, Box::new(self))
        }
        // there is a & so we will not affect the struct, just read it
        fn len(&self) -> u32 {
            match *self {
                // cant take ownership of the tail, because self is borrowed; instead take a reference to the tail
                Cons(_, ref tail) => 1 + tail.len(),
                // base case: an empty list has wero length
                Nil => 0,
            }
        }
        fn stringify(&self) -> String {
            match *self {
                Cons(head, ref tail) => {
                    // format! is similar to print! but returns a heap allocated string instead of printing to the console
                    format!("{}, {}", head, tail.stringify())
                },
                Nil => {
                    format!("Nil")
                }
            }
        }
    }
    let mut list = List::new();
    // prepend some elements
    list = list.prepend(1);
    list = list.prepend(2);
    list = list.prepend(3);
    // show the final state of the list
    println!("linked list has length: {}", list.len());
    println!("{}", list.stringify());
}

fn custom_types_constants() {
    // globals are declared outside all other scopes (not like here)
    static LANGUAGE: &str = "Rust";
    const THRESHOLD: i32 = 10;

    fn is_big(n: i32) -> bool {
        n > THRESHOLD
    }

    let n = 16;
    println!("This is {}", LANGUAGE);
    println!("The threshold is {}", THRESHOLD);
    println!("{} is {}", n, if is_big(n) { "big"} else { "small" });

    // Error! cannot modify a const.
    // THRESHOLD = 5;
    // FIXME ^ comment out this line
}

fn variable_bindings() {
    let an_integer = 1u32;
    let a_boolean = true;
    let unit = ();

    // copy an_integer into copied_integer
    let copied_integer = an_integer;

    println!("An integer: {:?}", copied_integer);
    println!("A boolean: {:?}", a_boolean);
    println!("meet the unit value: {:?}", unit);

    // the compiler wars about unused variable bindings; these warnings an be silenced by prefixign the variable name with an underscore
    let _unused_variable = 3u32;
    let _noisy_unused_variable = 2u32;
    // FIXME ^% prefix with an underscore to supppress the warning!
}

fn variable_bindings_mutability() {
    let _immutable_binding = 1;
    let mut mutable_binding = 1;
    println!("before mutation: {}", mutable_binding);
    // ok
    mutable_binding += 1;
    println!("after mutation: {}", mutable_binding);
    // error! comment me
    // _immutable_binding += 1;
}

fn variable_bindings_scope_and_shadowing() {
    let long_lived_binding = 1;
    {
        let short_lived_binding = 2;
        println!("inner short: {}", short_lived_binding);
    }
    // this does not work, out of scope
    // println!("outer short: {}", short_lived_binding);

    println!("outer long: {}", long_lived_binding);

    // ---- shadowing ----
    let shadowed_binding = 1;
    {
        println!("before being shadowed: {}", shadowed_binding);
        // this binding shadows the outer one
        let shadowed_binding = "abc";
        println!("shadowed in inner block: {}", shadowed_binding);
    }
    println!("outside inner block: {}", shadowed_binding);

    // this binding shadows the previous binding
    let shadowed_binding = 2;
    println!("shadowed in outer block: {}", shadowed_binding);
}

fn variable_bindings_declare_first() {
    // declare a variable binding
    let a_binding;
    {
        let x = 2;
        a_binding = x * x;
    }
    println!("a binding: {}", a_binding);
    
    let another_binding;
    // error, use of uninitialized binding
    // println!("another binding: {}", another_binding);
    another_binding = 2;
    println!("another binding: {}", another_binding);
}

fn variable_bindings_freezing() {
    let mut _mutable_integer = 7i32;
    {
        // shadowing the immutable _mutable_integer
        let _mutable_integer = _mutable_integer;
        // error! _mutable_integer is frozen in this scope
        // _mutable_integer = 50;
        // _mutable_integer goes out of scope
    }
    // ok! _mutable)integert is not frozen in this scope
    _mutable_integer = 3;
}

fn types_casting() {
    let decimal = 65.4321_f32;

    // error! no implicit conversion
    // let integer: u8 = decimal;

    // explicit conversions
    let integer = decimal as u8;
    let character = integer as char;

    // error! there are limitations in conversion rules
    // float cannot be directly converted to a char
    // let character = decimal as char;

    println!("casting: {} -> {} -> {}", decimal, integer, character);

    // when casting any value to an unsigned type, T, T::MAX + 1 is added or substracted until the value fits into the new type
    // 1000 already fits in a u16
    println!("1000 as a u16 is {}", 1000 as u16);
    // -1 + 256 = 255
    println!("-1 as a u8 iks {}", (-1i8) as u8);

    // for positive numbers, this is the same as the modulus
    println!("1000 mod 256 is {}", 1000 % 256);

    // when casting to a signed type the bitwise result is the same as first casting to the corresponding unsigned type. if the most significant bit of that value is 1, then the value is negative


    // unless it already fits, of course
    println!("128 as a i16 is {}", 128 as i16);
    // 128 as u8 whose two's complement in eight bits is:
    println!("128 as a i8 is {}", 128 as i8);

    // repeating the example  above
    // 1000 as a u8 is 232
    println!("1000 as u8 is {}", 1000 as u8);
    // and the two complenents of 232 is -24
    println!("232 as u8 is {}", 232 as i8);

    // since rust 1.45, the "as" keyword performs a saturating cast when casting from float to int
    // if the floating point value exceeds the upper bound or is less than the lower bound, the returned value will be equal to the bound crossed

    // 300.0 is 255
    println!("300.0 is {}", 300.0_f32 as u8);
    // -100.0 as u8 is 0
    println!("-100.0 as u8 is {}", -100.0_f32 as u8);
    // nan as u8 is 0
    println!("nan as u8 is {}", f32::NAN as u8);

    // this behavior incures a small runtame cost and can be avoided with unsafe methods, however the results might overflow and return **unsound values**. use these methods wisely:
    unsafe {
        // 300.0 is 44
        println!("300.0 is {}", 300.0_f32.to_int_unchecked::<u8>());
        // -100 as u8 is 156
        println!("-100.0 as u8 is {}", (-100.0_f32).to_int_unchecked::<u8>());
        // nan as u8 is 0
        println!("nan as u8 is {}", f32::NAN.to_int_unchecked::<u8>());
    }
}

fn types_literals() {
    // suffixed literals, their types are known at initialization
    let x = 1u8;
    let y = 2u32;
    let z = 3f32;

    // unsuffixed unsuffixed literals, their types depend on how they are used
    let i = 1;
    let f = 1.6;

    // size_of_val returns the size of a variable in bytes
    println!("size of x in byes: {}", std::mem::size_of_val(&x));
    println!("size of y in byes: {}", std::mem::size_of_val(&y));
    println!("size of z in byes: {}", std::mem::size_of_val(&z));
    println!("size of i in byes: {}", std::mem::size_of_val(&i));
    println!("size of f in byes: {}", std::mem::size_of_val(&f));
}

fn types_inference() {
    // because we annoted the compiler knows the type u8
    let elem = 5u8;
    // create an empty vector
    let mut vec = Vec::new();
    // at this point, the compiler doesnt know the exact type of vec, it just knows that its a vector of something
    vec.push(elem);
    // Aha! now the compiler knows that vec is a vector of u8's
    println!("{:?}", vec);
}

fn types_aliasing() {
    // nanosecond is a new name for u64
    type NanoSecond = u64;
    type Inch = u64;

    // use an attribute to silence warning.
    #[allow(non_camel_case_types)]
    type u64_t = u64;

    let nanoseconds: NanoSecond = 5 as u64_t;
    let inches: Inch = 2 as u64_t;

    // note that type aliases dont provide any extra type safety, because aliases are not new types
    println!(
        "{} nanoseconds + {} inches = {} units?",
        nanoseconds,
        inches,
        nanoseconds + inches
    );
}

fn convertion_from_and_into() {
    let my_str = "hello";
    let my_string = String::from(my_str);

    #[derive(Debug)]
    struct Number {
        value: i32,
    }

    impl From<i32> for Number {
        fn from(item: i32) -> Self {
            Number { value: item }
        }
    }

    let num = Number::from(30);
    println!("my number is {:?}", num);

    let int = 5;
    let num2: Number = int.into();
    println!("my number is {:?}", num2);
}

fn convertion_tryfrom_and_tryinto() {
    #[derive(Debug, PartialEq)]
    struct EvenNumber(i32);

    impl TryFrom<i32> for EvenNumber {
        type Error = ();

        fn try_from(value: i32) -> Result<Self, Self::Error> {
            if value % 2 == 0 {
                Ok(EvenNumber(value))
            } else {
                Err(())
            }
        }
    }

    // TryFrom
    assert_eq!(EvenNumber::try_from(8), Ok(EvenNumber(8)));
    assert_eq!(EvenNumber::try_from(5), Err(()));
    // TryInto
    let result: Result<EvenNumber, ()> = 8i32.try_into();
    assert_eq!(result, Ok(EvenNumber(8)));
    let result: Result<EvenNumber, ()> = 5i32.try_into();
    assert_eq!(result, Err(()));
}

fn convertion_to_and_from_strings() {
    struct Circle {
        radius: i32,
    }
    impl fmt::Display for Circle {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Circle of radius {}", self.radius)
        }
    }
    let circle = Circle { radius: 6 };
    println!("{}", circle.to_string());

    let parsed: i32 = "5".parse().unwrap();
    let turbo_parsed = "10".parse::<i32>().unwrap();

    let sum = parsed + turbo_parsed;
    println!("Sum: {:?}", sum);
}

fn expressions() {
    // variable binding
    let x = 5;
    // expression;
    x;
    x + 1;
    15;

    let x = 5u32;
    let y = {
        let x_squared = x * x;
        let x_cube = x_squared * x;
        // this expression will be assigned to y
        x_cube + x_squared + x
    };
    let z = {
        // the semicolon suppresses this expression and () is assigned to z
        2 * x;
    };

    println!("x is {:?}", x);
    println!("y is {:?}", y);
    println!("z is {:?}", z);
}

fn flowcontrol_ifelse() {
    let n = 5;
    if n < 0 {
        print!("{} is negative", n);
    } else if n > 0 {
        print!("{} is positive", n);
    } else {
        print!("{} is zero", n);
    }

    let big_n = if n < 10 && n > -10 {
        println!(", and is a small number, increase ten-fold");
        10 * n
    } else {
        println!(", and is a big number, halve the number");
        n / 2
    };
    println!("{} -> {}", n, big_n);
}

fn flowcontrol_loop() {
    let mut count = 0u32;
    println!("Lets count until infinity!");
    // infinite loop
    loop {
        count += 1;
        if count == 3 {
            println!("three!");
            continue;
        }
        println!("{}", count);
        if count == 5 {
            println!("ok, thats enough");
            break;
        }
    }

    'outer: loop {
        println!("Entered the outer loop!");
        'inner: loop {
            println!("entered the inner loop");
            break 'outer;
        }
        println!("this point will never be reached");
    }
    println!("exited the outer loop.");

    // --
    let mut counter = 0;
    let result = loop {
        counter += 1;
        if counter == 10 {
            break counter * 2;
        }
    };
    assert_eq!(result, 20);
}

fn flowcontrol_while() {
    let mut n = 1;
    while n < 101 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
        n += 1;
    }
}

fn flowcontrol_forloops() {
    for n in 1..101 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }

    for n in 1..=100 {
        // same as above
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }

    // iter
    let names = vec!["bob", "frank", "ferris"];
    for name in names.iter() {
        match name {
            &"ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }

    for name in names.into_iter() {
        match name {
            "ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }

    let mut names2 = vec!["bob", "frank", "ferris"];
    for name in names2.iter_mut() {
        *name = match name {
            &mut "ferris" => "there is a rustacean among us!",
            _ => "Hello",
        }
    }
    println!("names: {:?}", names2);
}

fn flowcontrol_match() {
    let number = 13;
    println!("tell me about {}", number);
    match number {
        1 => println!("one!"),
        2 | 3 | 5 | 7 | 11 => println!("this is a prime"),
        13..=19 => println!("a teen"),
        _ => println!("aint special"),
    }

    let boolean = true;
    let binary = match boolean {
        false => 0,
        true => 1,
    };
    println!("{} -> {}", boolean, binary);

    // destructuring
    // tuples destructuring
    let triple = (0, -2, 3);
    println!("tell me about {:?}", triple);
    match triple {
        (0, y, z) => println!("first is 0, y is {:?} and z is {:?}", y, z),
        (1, ..) => println!("first is 1, and the rest doesnt matter"),
        // .. can be used to ignore the rest of the tuple
        _ => println!("it doesnt matter what they are"),
    }
    // enums destructuring
    enum Color {
        Red,
        Blue,
        Green,
        RGB(u32, u32, u32),
        HSV(u32, u32, u32),
        HSL(u32, u32, u32),
        CMY(u32, u32, u32),
        CMYK(u32, u32, u32, u32),
    }

    let color = Color::RGB(122, 17, 40);
    println!("what color is it?");
    match color {
        Color::Red => println!("the color is red!"),
        Color::Green => println!("the color is Green!"),
        Color::Blue => println!("the color is Blue!"),
        Color::RGB(r, g, b) => println!("red {} green {} blue {}", r, g, b),
        Color::HSV(h, s, v) => println!("Hue {} Saturation {} Value {}", h, s ,v),
        Color::HSL(h, s, l) => println!("Hue {} Saturation {} Lightness {}", h, s, l),
        Color::CMY(c, m, y) => println!("cyan {} magenta {} yellow {}", c,m,y),
        Color::CMYK(c, m, y, k) => println!("cyan {} magenta {} yellow {} key (black): {}", c,m,y,k),
    }

    // pointer/ref destructuring
    let reference = &4;
    match reference {
        &val => println!("got a value via destructuring: {:?}", val),
    }
    // dereferencing here to avoid the '&' in the matches
    match *reference {
        val => println!("got a value via dereferencing: {:?}", val),
    }

    // what if you dont start with a ref? ref was a & because the right side was already a ref. this is not a ref because the right side is not one
    let _not_a_ref = 3;
    // but rust privides ref for exactly this purpose. it modifies the assignment so that a ref is created for the element. this ref is assigned
    let ref _is_a_ref = 3;
    // accordingly, by defining 2 values without refs, refs can be retrieved via "ref" and "ref mut"
    let value = 5;
    let mut mut_value = 6;

    // use ref keyword to create a ref
    match value {
        ref r => println!("got a ref to a val: {:?}", r),
    }
    // use ref mut similarly
    match mut_value {
        ref mut m => {
            *m += 10;
            println!("we added 10. mut_value: {:?}", m);
        },
    }
    // struct destructuring
    struct Foo {
        x: (u32, u32),
        y: u32,
    }
    let foo = Foo { x: (1, 2), y: 3 };
    match foo {
        Foo { x: (1, b), y } => println!("first of x is 1, b = {}, y = {}", b, y),
        // you can destructure structs and rename variables, the order is not important
        Foo { y: 2, x: i } => println!("y is 2, i = {:?}", i),
        // and you can also ignore some variables:
        Foo { y, .. } => println!("y = {}, we dont care about x", y),
        // this will give an error: pattern does not mention field x
        // Foo { y } => println!("y = {}", y);
    }

    // guards
    let pair = (2, -2);
    print!("tell me about {:?}", pair);
    match pair {
        (x, y) if x == y => println!("those are twins"),
        // the ^  if condition part is a guard
        (x, y) if x + y == 0 => println!("antimatter, kaboom!"),
        (x, _) if x % 2 == 1 => println!("the first one is odd"),
        _ => println!("no correlation..."),
    }

    // binding
    fn age() -> u32 {
        15
    }

    println!("tell me what type of person you are");
    match age() {
        0 => println!("i havent celebrated my first birtday yet"),
        // could match 1.. =12 directly but then what age whould the child be? instead, bind to N for the sequence of 1..=12
        n @ 1 ..=12 => println!("i am a child of age {:?}", n),
        n @ 13..=19 => println!("i am a teen of age {:?}", n),
        // nothing is bound, return the result
        n => println!("i am an old person of age {:?}", n),
    }

    fn some_number() -> Option<u32> {
        Some(42)
    }
    match some_number() {
        // got Some variant match if its value, bound to n, equals 42
        Some(n @ 42) => println!("the answer: {}", n),
        Some(n) => println!("not interesting... {}", n),
        _ => (),
    }
}

fn flowcontrol_iflet() {
    let optional = Some(7);
    match optional {
        Some(i) => {
            println!("this is a really long string and {:?}", i);
        },
        _ => (),
    }

    let number = Some(7);
    let letter: Option<i32> = None;
    let emoticon: Option<i32> = None;

    // the if let construct reads: if "let" destructures "number" into "some(i), evaluate the block {}"
    if let Some(i) = number {
        println!("matched {:?}", i);
    }

    //if you need to specify a failure, use an else
    if let Some(i) = letter {
        println!("matched {:?}", i);
    } else {
        println!("didnt match a number, lets go with a letter!");
    }

    // provide an altered failing condition
    let i_like_letters = false;
    if let Some(i) = emoticon {
        println!("matched {:?}", i);
    } else if i_like_letters {
        println!("didnt match a number, lets go with a letter");
    } else {
        println!("i dont like letters. lets go with an emoticon :)");
    }

    enum Foo {
        Bar,
        Baz,
        Qux(u32),
    }

    let a = Foo::Bar;
    let b = Foo::Baz;
    let c = Foo::Qux(100);

    // variable a matches foo::bar
    if let Foo::Bar = a {
        println!("a is foobar");
    }

    // variable b does not match Foo::bar so it will print nothing
    if let Foo::Bar = b {
        println!("b is foobar");
    }

    // variable c matches Foo::qux which has a value similar to some)_ in the previous example
    if let Foo::Qux(value) = c {
        println!("c is {}", value);
    }

    // binding also works with if let
    if let Foo::Qux(value @ 100) = c {
        println!("c is one hundred");
    }

    // challenge
    // this enum purposely neither implements nor derives PartialEq.
    // this is why comparing Foo::Bar == a fails below
    enum Foo2 { Bar }
    let a = Foo2::Bar;

    // variable a matches Foo::Bar
    if let Foo2::Bar = a {
        // this causes a compile-time error, use if let instead
        println!("a is foobar");
    }
}

fn flowcontrol_whilelet() {
    // boring way with match example
    let mut optional = Some(0);
    loop {
        match optional {
            Some(i) => {
                if i > 9 {
                    println!("greater than 9, quit!");
                    optional = None;
                } else {
                    println!("i is {:?}. try again!", i);
                    optional = Some(i + 1);
                }
            },
            _ => { break; },
        }
    }
    // can be better with while let:
    let mut optional = Some(0);
    // this reads: while let destructures optional, into some(i), evaluate the block {}. else break;
    while let Some(i) = optional {
        if i > 9 {
            println!("greater than 9, quit!");
            optional = None;
        } else {
            println!("i is {:?}. try again", i);
            optional = Some(i + 1);
        }
    } // IF LET has optional else, but not While let
}

fn functions() {
    fn is_divisible_by(lhs: u32, rhs: u32) -> bool {
        if rhs == 0 {
            return false;
        }
        lhs % rhs == 0
    }
    fn fizzbuzz(n: u32) -> () {
        if is_divisible_by(n, 15) {
            println!("fizzbuzz");
        } else if is_divisible_by(n, 3) {
            println!("fizz");
        } else if is_divisible_by(n, 5) {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }

    fn fizzbuzz_to(n: u32) {
        for n in 1..n + 1 {
            fizzbuzz(n);
        }
    }

    fizzbuzz_to(100);
}

fn functions_methods() {
    struct Point {
        x: f64,
        y: f64,
    }

    impl Point {
        fn origin() -> Point {
            Point { x: 0.0, y: 0.0 }
        }
        fn new(x: f64, y: f64) -> Point {
            Point { x, y }
        }

    }

    struct Rectangle {
        p1: Point,
        p2: Point,
    }

    impl Rectangle {
        fn area(&self) -> f64 {
            let Point { x: x1, y: y1 } = self.p1;
            let Point { x: x2, y: y2 } = self.p2;
            // abs ais a f64 method returning absolute val of caller
            ((x1 - x2) * (y1 - y2)).abs()
        }
        fn perimeter(&self) -> f64 {
            let Point { x: x1, y: y1 } = self.p1;
            let Point { x: x2, y: y2 } = self.p2;
            2.0 * ((x1 - x2).abs() + (y1 - y2).abs())
        }

        // this method requires the caller to be mutable
        fn translate(&mut self, x: f64, y: f64) {
            self.p1.x += x;
            self.p2.x += x;

            self.p1.y += y;
            self.p2.y += y;
        }
    }

    struct Pair (Box<i32>, Box<i32>);

    impl Pair {
        // this method consumes the resources of the caller object
        fn destroy(self) {
            // destructure self
            let Pair(first, second) = self;
            println!("Destroying pair ({}, {})", first, second);
        }
    }

    // static methods are called using double colons
    let rectangle = Rectangle {
        p1: Point::origin(),
        p2: Point::new(3.0, 4.0),
    };

    // instance methods are called using the dot operator
    // note that the first argument &self is implicitly passed,
    // i.e. rectangle.perimeter() 
    println!("Rectangle perimeter: {}", rectangle.perimeter());
    println!("Rectangle area: {}", rectangle.area());

    let mut square = Rectangle {
        p1: Point::origin(),
        p2: Point::new(1.0, 1.0),
    };
    // error!: rectangle is imutable, but this method requires a mutable object
    // rectangle.translate(1.0, 0.0);

    // mutable object can call mutable methods
    square.translate(1.0, 1.0);
    let pair = Pair(Box::new(1), Box::new(2));
    pair.destroy();
    // error! previous destroy call consumed pair
    // pair.destroy();
}

fn functions_closures() {
    // increment via closures and functions
    fn function (i: i32) -> i32 { i + 1 }
    // closures are anonymous, here we are binding them to references
    // annotation is identical to function annotation but is optional as are the {}  wrapping the body.
    // these nameless functions are assigned to appropriately named variables.
    let closure_annotated = |i: i32| -> i32 { i + 1 };
    let closure_inferred = |i| i+1;
    let i = 1;
    println!("function: {}", function(i));
    println!("closure annotated: {}", closure_annotated(i));
    println!("closure inferred: {}", closure_inferred(i));

    // a closure taking no arguments which returns an i32
    // the return type is inferred.
    let one = || 1;
    println!("closure returning one: {}", one());
}

fn functions_closures_capturing() {
    use std::mem;
    let color = String::from("green");
    // a closure to print color which immediately borros (&) color and stores the borrow and closure in the print variable. it will remain borrowed until print is used the last time.
    let print = || println!("color: {}", color);
    // call the closure using the borrow
    print();
    // color can be borrowed immutably again, because the closure only holds an immutable reference to color
    let _reborrow = &color;
    print();
    // a move or reborrow is allowed after the final use of print
    let _color_moved = color;

    let mut count = 0;
    // a closure to increment count could take either &mut count or count but &mut count is less restrictive so it takes that; immediately borrows count
    // a mut is required on inc because a &mut is stored inside. thus, calling the closure mutates the closure which requires a mut.
    let mut inc = || {
        count += 1;
        println!("count: {}", count);
    };
    // call the closure using a mutable borrow
    inc();
    // the closure still mutably borrows count because it is called later.
    // an attempt to reborrow will lead to an error
    // let _reborrow = &count;
    inc();

    // the closure no longer needs to borrow &mut count. therefore it is possible to reborrow without an error
    let _count_reborrowed = &mut count;

    // a non-copy type
    let movable = Box::new(3);
    // mem::drop requires T so this must take by value; A copy type would copy into the closure leaving the original untouched.
    // a non-copy must move and so movable immediately moves into the closure.
    let consume = || {
        println!("movable: {:?}", movable);
        mem::drop(movable);
    };

    // consume consumes the variable so this can only be called once
    consume();
    // consume();

    // Vec has non-copy semantics
    let haystack = vec![1, 2, 3];
    let contains = move |needle| haystack.contains(needle);
    println!("{}", contains(&1));
    println!("{}", contains(&4));
    // println!("there're {} elements in vec", haystack.len());
    // removing 'move' from closures signature will cause closure to borrow haystack variable immutable, hence haystack is still available and uncommenting above line will not cause an error
}

fn functions_closures_asinputparameters() {
    // a function which takes a closure as an argument and calls it
    // <F> denotes that F is a generic type parameter
    fn apply<F>(f: F) where
        // the closure takes no input and returns nothing
        F: FnOnce() {
            // todo try changing this to Fn or FnMut
            f();
        }
    // a function which takes a closure and returns an i32
    fn apply_to_3<F>(f: F) -> i32 where
        // a closure takes a i32 and returns a i32
        F: Fn(i32) -> i32 {
            f(3)
        }
    
    use std::mem;
    let greeting = "hello";
    // a non-copy type
    // to_owned creates owned data from borrowed one
    let mut farewell = "goodbye".to_owned();

    // capture 2 variables: greeting by ref and farewell by value
    let diary = || {
        // greeting is by ref: requires Fn
        println!("I said {}", greeting);
        // mutation forces farewell to be captured by mutable ref. now requires FnMut
        farewell.push_str("!!!");
        println!("then i screamed {}", farewell);
        println!("now i can sleep. zzzzz");

        mem::drop(farewell);
    };

    apply(diary);
    // double satisfies apply_to_3's trait bound
    let double = |x| 2*x;
    println!("3 doubled: {}", apply_to_3(double));
}

fn functions_closures_typeanonymity() {
    // f must be generic
    fn apply1<F>(f: F) where
        F: FnOnce() {
            f();
        }
        // f must implement Fn for a closure which takes no inptus and returns nothing - exactly what is required for print.
    fn apply2<F>(f: F) where
        F: Fn() {
            f();
        }
    let x = 7;
    // capture the x into an anonymous type and implement Fn for it. Store it in print
    let print = || println!("{}", x);
    apply2(print);
}

fn functions_closures_inputfunctions() {
    // define a function which takes a generic F argument
    // bounded by Fn, and calls it
    fn call_me<F: Fn()>(f: F) {
        f();
    }

    // define a wrapper function satisfying the Fn bound
    fn function() {
        println!("im a function!");
    }

    // define a closure satisfying the Fn bound
    let closure = || println!("im a closure!");

    call_me(closure);
    call_me(function);
}

fn functions_closures_asoutputparameters() {
    fn create_fn() -> impl Fn() {
        let text = "Fn".to_owned();
        move || println!("this is a: {}", text)
    }
    fn create_fnmut() -> impl FnMut() {
        let text = "FnMut".to_owned();
        move || println!("this is a: {}", text)
    }
    fn create_fnonce() -> impl FnOnce() {
        let text = "FnOnce".to_owned();
        move || println!("this is a: {}", text)
    }

    let fn_plain = create_fn();
    let mut fn_mut = create_fnmut();
    let fn_once = create_fnonce();

    fn_plain();
    fn_mut();
    fn_once();
}

fn functions_closures_instd() {
    let vec1 = vec![1, 2, 3];
    let vec2 = vec![4, 5, 6];

    // iter() for vecs yields &i32. destructure to i32.
    println!("2 in vec1: {}", vec1.iter().any(|&x| x ==2));
    // into_iter() for vecs yields i32. no destructuring required.
    println!("2 in vec2: {}", vec2.into_iter().any(|x| x == 2));

    let array1 = [1, 2, 3];
    let array2 = [4, 5, 6];

    // iter for arrays ytields &i32
    println!("2 in array1: {}", array1.iter().any(|&x| x == 2));
    // into_iter for arrays unusually yields &i32
    println!("2 in array2: {}", array2.into_iter().any(|&x| x == 2));


    // -----
    // searching through iterators
    // -----

    let vec1 = vec![1, 2, 3];
    let vec2 = vec![4, 5, 6];

    // iter yields &i32
    let mut iter = vec1.iter();
    // into_iter yields i32
    let mut into_iter = vec2.into_iter();

    // iter for vec yields &i32, and we want to ref one of its items, si we have to destructure &&i32 to i32
    println!("Find 2 in vec1 {:?}", iter.find(|&&x| x == 2));
    // into_iter for vecs yields i32, and we want to ref one of its items, so we have to destruct &i32 to i32
    println!("Fiond 2 in vec2 {:?}", into_iter.find(|&x| x == 2));

    let array1 = [1, 2, 3];
    let array2 = [4, 5, 6];

    // iter for arrays yields &i32
    println!("Find 2 in array1 {:?}", array1.iter().find(|&&x| x == 2));
    // into_iter for arrays unusually yields &i32
    println!("Find 2 in array2 {:?}", array2.iter().find(|&&x| x == 2));

    let vec = vec![1, 9, 3, 3, 13, 2];
    let index_of_first_even_number = vec.iter().position(|x| x %2 == 0);
    assert_eq!(index_of_first_even_number, Some(5));
    let index_of_first_negative_number = vec.iter().position(|x| x < &0);
    assert_eq!(index_of_first_negative_number, None);
}

fn functions_higher_order_functions () {
    fn is_odd(n: u32) -> bool {
        n % 2 == 1
    }
    println!("Find the sum of all the squared odd numbers under 1000");
    let upper = 1000;
    // imperative approach
    // declare accumulator variable
    let mut acc = 0;
    // iterate: 0, 1, 2, .. to infinity
    for n in 0.. {
        // square the number
        let n_squared = n*n;

        if n_squared >= upper {
            break;
        } else if is_odd(n_squared) {
            acc += n_squared;
        }
    }
    println!("imperative style: {}", acc);

    // functional approach
    let sum_of_squared_odd_numbers: u32 =
        (0..).map(|n| n*n) // all natural numbers squared
            .take_while(|&n_squared| n_squared < upper) // below upper limit
            .filter(|&n_squared| is_odd(n_squared)) // that are odd
            .fold(0, |acc, n_squared| acc + n_squared); // sum them
    println!("functional style: {}", sum_of_squared_odd_numbers);
}

fn functions_divergingfunctions() {
    fn foo() -> ! {
        panic!("this call never returns.");
    }

    fn some_fn() {
        ()
    }
    let a: () = some_fn();
    println!("this function returns and you can see this line.");
    let x = panic!("this call never returns.");
    println!("you will never see this line!");

    fn some_odd_number(up_to: u32) -> u32 {
        let mut acc = 0;
        for i in 0..up_to {
            // notice that the return type of this match expression must be u32
            // because of the type of the addition variable
            let addition: u32 = match i%2 == 1 {
                true => i,
                false => continue,
            };
            acc += addition;
        }
        acc
    }
    println!("sum of the odd numbers up to 9 (excluding): {}", some_odd_number(9));
}

// modules
mod my_mod {
    // items in modules default to private visibility
    fn private_function() {
        println!("called my_mod::private_function()");
    }

    // use the "pub" modifier to override default visibility
    pub fn function () {
        println!("called my_mod::function()");
    }
    
    // items can access other items in the same module, even when private
    pub fn indirect_access() {
        print!("called my_mod::indirect_access(), that\n> ");
        private_function();
    }

    // modules can also be nested
    pub mod nested {
        pub fn function () {
            println!("called my_mod::nested::function()");
        }

        fn private_function() {
            println!("called my_mod::nested::private_function()");
        }

        // functions declared using "pub(in path)" syntax are only visible within the given path.
        // path must be a parent or ancestor module
        pub(in crate::my_mod) fn public_function_in_my_mod() {
            print!("called `my_mod::nested::public_function_in_my_mod()`, that\n> ");
            public_function_in_nested();
        }

        // functions declared using 'pub(self)' syntax are only visible within the current mdoule, which is the same as leaving them private
        pub(self) fn public_function_in_nested() {
            println!("called my_mod::nested::public_function_in_nested()");
        }

        // functions declared using 'pub(super)'  syntax are only visible within the parent module
        pub(super) fn public_function_in_super_mod() {
            println!("called my_mod::nested::public_function_in_super_mod()");
        }
    }

    pub fn call_public_function_in_my_mod() {
        print!("called `my_mod::call_public_function_in_my_mod()`, that\n> ");
        nested::public_function_in_my_mod();
        print!("> ");
        nested::public_function_in_super_mod();
    }

    // pub(crate) makes functions visible only within the current crate
    pub(crate) fn public_function_in_crate() {
        println!("called my_mod::public_function_in_crate()");
    }

    // nested modules follow the same rules for visibility
    mod private_nested {
        pub fn function () {
            println!("called my_mod::private_nested::functio()");
        }

        // private parent items will still restrict the visibility of a child item,
        // even if it is declared as visible within a bigger scope
        pub(crate) fn restricted_function() {
            println!("called my_mod::private_nested::restricted_fuinction()");
        }
    }
}

fn mod_visibility_function() {
    println!("called function()");
}

fn mod_visibility() {
    mod_visibility_function();
    my_mod::function();
    // public items including those iside nested modules can be accessed from outside the parent module
    my_mod::indirect_access();
    my_mod::nested::function();
    my_mod::call_public_function_in_my_mod();
    // pub(crate) items can be called from anywhere in the same crate
    my_mod::public_function_in_crate();

    // pub(in path) items can only be called from within the module specified
    // error: private
    // my_mod::nested::public_function_in_my_mod();

    // private items of a module cannot be directly accessed, even if nested in a public module
    // my_mod::private_function();
    // my_mod::nested::private_function();
    // my_mod::private_nested::function();
    // my_mod::private_nested::restricted_function();
}

mod my {
    // a public struct with a public field of generic type T
    pub struct OpenBox<T> {
        pub contents: T,
    }

    // a public struct with a private field of generic type T
    pub struct ClosedBox<T> {
        contents: T,
    }

    impl<T>  ClosedBox<T> {
        // a public constructor method
        pub fn new(contents: T) -> ClosedBox<T> {
            ClosedBox {
                contents: contents,
            }
        }
    }
}

fn mod_structvisibility() {
    // public structs with public fields can be constructed as usual
    let open_box = my::OpenBox { contents: "public information" };
    // and their fields can be normally accessed
    println!("the open box contains: {}", open_box.contents);

    // public structs with private fields cannot be constructed using field names
    // let closed_box = my::ClosedBox { contents: "classified info" };
    // however, structs with private fields can be created using public constructors
    let _closed_box = my::ClosedBox::new("classified");
    // and the private fields of a public struct cannot be accessed
    // println!("the closed box contains {}", _closed_box.contents);
}

fn mod_use_function(){
    println!("called function");
}

mod deeply {
    pub mod nested {
        pub fn function() {
            println!("called deeply::nested::function");
        }
    }
}

use deeply::nested::function as mod_use_other_function;
fn mod_use() {
    // easier access
    mod_use_other_function();
    println!("entering block");
    {
        // this is the equivalent to use deeply::nested::functionm as function
        // this function() will shadow the outer one
        use crate::deeply::nested::function as mod_use_function;
        
        // 'use' bindings have a local scope. in this case, the
        // shadoiwing of function is onlky in this block.
        mod_use_function();

        println!("leaving block");
    }
    mod_use_function();
}

fn mod_superself_function() {
    println!("called function");
}

mod cool {
    pub fn mod_superself_function() {
        println!("called cool::function");
    }
}

mod my2 {
    use crate::mod_visibility_function;

    fn mod_superself_function() {
        println!("called my::function");
    }

    mod cool {
        pub fn mod_superself_function() {
            println!("called my::cool::function");
        }
    }

    pub fn indirect_call() {
        print!("called my::indirect_call, that n > ");
        // the self keyword refers to the current module scope
        // in this case 'my'. calling self::function and calling function() directly both give the same result, but they refer to the sxame function
        self::mod_superself_function();
        mod_superself_function();

        // we can also use self to access another module inside my
        self::cool::mod_superself_function();
        // the super keyword refers to the parent scole (outside the 'my' module)
        super::mod_superself_function();

        // this will bind to the cool::function in the crate scope
        // in this case, the crate scope is the outermost scope
        {
            use crate::cool::mod_superself_function as root_function;
            root_function();
        }
    }
}

fn mod_superself() {
    my2::indirect_call();
}

fn bounds_printer<T: Display>(t: T) {
    println!("{}", t);
}
struct S<T: Display>(T);

fn bounds () {
    // let s = S(vec![1]);
    // ^ gives error as vec does not implements Display
}

fn main() {
    mod_superself();
}
Wi