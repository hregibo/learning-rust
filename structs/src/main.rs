fn main() {
    // if not 'mut', using 'user1.email' won't work
    let mut user1 = User {
        email: String::from("gitlab@lumikko.dev"),
        username: String::from("lumikkode"),
        active: true,
        sign_in_count: 1,
    };
    user1.email = String::from("another@lumikko.dev");

    // example of "update" syntax for a struct
    let user2 = User {
        email: String::from("Hello@motorola.com"),
        username: String::from("Motorola Support"),
        ..user1 // spread operator (but only two dots, like slice)
    };

    let black = TupleStruct(0, 0, 0);
    println!("black is R{} G{} B{}", black.0, black.1, black.2);

    // example of program using structs
    let width1 = 30;
    let height1 = 50;

    println!(
        "The area of the rectangle is {} square pixels.",
        area_params(width1, height1)
    );

    // we dont really know that width & height are related
    // to do this, we use a tuple
    let rect1 = (30, 50);
    println!(
        "The area of the rectangle is {} square pixels.",
        area_tuple(rect1)
    );

    // tuples group them, but makes unclear what is width/height
    // lets use a struct instead
    let rect2 = Rectangle {
        width: 30,
        height: 50,
    };
    println!(
        "The area of the rectangle is {} square pixels.",
        area_rectangle(&rect2)
    );
    println!("rect2 is {:#?}", rect2);
    println!("the area of the rectangle is {} square pixels", rect2.area());
    println!("can rect1 hold rect2? {}", rect2.can_hold(&rect2));

    let sq = Rectangle::square(1);
    println!("rect square: {:#?}", sq);
}
#[derive(Debug)] // allows us to use {:#?} format in println!
struct Rectangle {
    width: u32,
    height: u32,
}
impl Rectangle {
    fn area (&self) -> u32 {
        self.width * self.height
    }
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}
fn area_rectangle(rect: &Rectangle) -> u32 {
    rect.width * rect.height
}
fn area_tuple(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}
fn area_params(width: u32, height: u32) -> u32 {
    width * height
}


struct TupleStruct(i32, i32, i32);
struct UnitStruct {}

struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: String, username: String) -> User {
    // just like JS/TS, if var = key, just putting var name works
    User {
        email, 
        username,
        active: true,
        sign_in_count: 1,
    }
}
