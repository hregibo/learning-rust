extern crate libxdo;
use libxdo::XDo;
use std::time::Duration;
fn main() {
    std::thread::sleep(Duration::from_millis(3000));
    println!("Starting");
    let x11 = XDo::new(None).unwrap();
    loop {
        x11.send_keysequence("g", 50).unwrap(); // placard
        std::thread::sleep(Duration::from_millis(100));
        x11.send_keysequence("g", 50).unwrap(); // open popup
        std::thread::sleep(Duration::from_millis(1000));
        x11.send_keysequence("g", 50).unwrap(); // click "buy"
        std::thread::sleep(Duration::from_millis(200));
        x11.send_keysequence("g", 50).unwrap(); // click "personal housing"
        std::thread::sleep(Duration::from_millis(500));
        x11.send_keysequence("f", 100).unwrap(); // move cursor left->"confirm"
        std::thread::sleep(Duration::from_millis(400));
        x11.send_keysequence("g", 0).unwrap(); // confirm purchase
        std::thread::sleep(Duration::from_millis(1000));
    }
}
