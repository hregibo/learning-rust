fn main() {
    let _guard = sentry::init("https://1a642c393dcb4a0489e057e3b4215835@sentry.lumikko.dev/7");
    println!("Hello, world!");
    sentry::capture_message("Everything is on fire!", sentry::Level::Info);
}
