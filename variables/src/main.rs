const MAX_POINTS: u32 = 100_000;

fn main() {
  // mutable
  let mut x = 5;
  println!("value of x is {}", x);
  x = 6;
  println!("value of x is {}", x);

  // immutable but shadowed due to "let"
  let y = 5;
  println!("value of y is {}", y);
  let y = y+1;
  println!("value of y is {}", y);

  // shadowing allows to change type where mut keeps type
  // but shadowing creates a new variable with same name
  let spaces = "          ";
  let spaces = spaces.len();

  let tuple: (u32, String, bool) = (500, String::from("Hello"), false);
  let val_2: String = tuple.1;
  // implicit type & size
  let arr = [1, 2, 4];
  // explicit type & size
  let arrTypeSize: [i32; 5] = [1, 2, 3, 4, 5];
  // repeated value 5 times
  let arrSameVal = [3; 5];
  let accessed_value = arrTypeSize[4];
}
