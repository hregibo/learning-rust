use std::io;

fn read_temp_type()-> String {
  let mut temp_type = String::new();
  while temp_type != "c" && temp_type != "f" {
    println!("Please enter 'c' if your temperature is in Celcius (will be converted to Farenheit)");
    println!("Please enter 'f' if your temperature is in Farenheit (will be converted to Celcius)");
    println!("Enter your choice (c/f):");
    io::stdin()
      .read_line(&mut temp_type)
      .expect("failed to parse input");
    temp_type = String::from(temp_type.trim());
  }
  temp_type
}

fn read_temp_value() -> f32 {
  loop {
    let mut temp_value = String::new();
    println!("Please enter the temperature you need to convert. If there is a decimal, please use the dot (.) to separate them from natural number. Your temperature:");
    io::stdin()
      .read_line(&mut temp_value)
      .expect("Failed to parse temperature value");
    let temp_value: f32 = match temp_value.trim().parse() {
      Ok(num) => num,
      Err(_) => continue,
    };
    return temp_value;
  };
}

fn get_cli_temp_type_or_ask(p: &String) -> String {
  if String::from("-c").eq_ignore_ascii_case(&p) {
    return String::from("c");
  } else if String::from("-f").eq_ignore_ascii_case(&p) {
    return String::from("f");
  }
  read_temp_type()
}

fn get_cli_temp_value_or_ask(p: &String) -> f32 {
  match p.parse() {
    Err(_) => read_temp_value(),
    Ok(val) => val,
  }
}

fn main() {
  let args: Vec<String> = std::env::args().collect();
  let temperature_type = match args.len() {
    3 => get_cli_temp_type_or_ask(&args[1]),
    _ => read_temp_type(),
  };
  let temperature_value = match args.len() {
    3 => get_cli_temp_value_or_ask(&args[2]),
    _ => read_temp_value(),
  };
  if temperature_type == "c" {
    println!("{}°C => {}°F", temperature_value, c_to_f(temperature_value))
  } else {
    println!("{}°F => {}°C", temperature_value, f_to_c(temperature_value))
  }
}

fn c_to_f(c: f32) -> f32 {
  (c * (9.0/5.0)) + 32.0
}

fn f_to_c(f: f32) -> f32 {
  (f - 32.0) * (5.0/9.0)
}