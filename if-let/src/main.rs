#[derive(Debug)]
enum USState {
    Alabama,
}
enum Coins {
    Quarter(USState),
    Penny,
}

fn main() {
    let some_u8_value = Some(0u8);
    match some_u8_value {
        Some(3) => println!("three"),
        _ => (),
    }
    // only caring for one value,
    // the following is better:
    if let Some(3) = some_u8_value {
        println!("three");
    }

    let mut count = 0;
    let coin1 = Coins::Quarter(USState::Alabama);
    let coin2 = Coins::Quarter(USState::Alabama);
    match coin1 {
        Coins::Quarter(state) => println!("state quarter from {:?}", state),
        _ => count += 1,
    }
    // can be used this way instead
    let mut count = 0;
    if let Coins::Quarter(state) = coin2 {
        println!("state of quarter: {:?}", state);
    } else {
        count += 1;
    }
}
