fn aborting() {
    /*  add following to cargo.toml to show aborting
        (letting OS clear memory, instead of "unwinding" memory assignations)
        [profile.release]
        panic = 'abort'
    */
    panic!("aborting");
}

fn bug_oob() {
    let v = vec![1, 2, 3];
    v[99];
}

use std::fs::File;
fn result_openfile() {
    let f = File::open("/tmp/ztest.txt");
    let f = match f {
        Ok(file) => file,
        Err(error) => panic!("Failed to open file {:?}", error),
    };
    println!("{:?}", f);
}

use std::io::ErrorKind;
fn result_openfile_errorspecific() {
    let f = File::open("/tmp/test.txt");
    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("/tmp/hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Failed to create file"),
            },
            other_error => {
                panic!("Problem opening file: {:?}", other_error);
            },
        },
    };
}

fn seasoned_rustacean_error() {
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
}

fn main() {
    // aborting()
    // bug_oob()
    // result_openfile()
    // result_openfile_errorspecific()
    seasoned_rustacean_error();
}
