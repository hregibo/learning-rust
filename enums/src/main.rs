fn main() {
    let four = IpAddrKind01::V4;
    let six = IpAddrKind01::V6;

    route01(four);
    route01(six);

    let home01 = IpAddr01 {
        kind: IpAddrKind01::V4,
        address: String::from("127.0.0.1"),
    };
    let loopback01 = IpAddr01 {
        kind: IpAddrKind01::V6,
        address: String::from("::1"),
    };

    // -
    let home02 = IpAddr02::V4(String::from("127.0.0.1"));
    let loopback02 = IpAddr02::V6(String::from("::1"));

    let home03 = IpAddr03::V4(127, 0, 0, 1);
    let loopback03 = IpAddr03::V6(String::from("::1"));
}

enum IpAddrKind01 {
    V4,
    V6,
}

fn route01(ip_kind: IpAddrKind01) {}

struct IpAddr01 {
    kind: IpAddrKind01,
    address: String,
}

enum IpAddr02 {
    V4(String),
    V6(String),
}

enum IpAddr03 {
    V4(u8,u8,u8,u8),
    V6(String),
}

struct ipv4Addr {
    // -- something
}
struct ipv6Addr {
    // -- something
}
enum IpAdr04 {
    V4(ipv4Addr),
    V6(ipv6Addr),
}
