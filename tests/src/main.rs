fn main() {
    // string type ownership
    // via heap
    let mut s = String::from("heyo");
    s.push_str(" there");
    println!("{}", s);

    // works as intended, does a copy
    let x = 5;
    let y = x;
    println!("{} {}", x, y);

    // changes ownership, rendering old owner invalid
    let s1 = String::from("hi");
    let s2 = s1;
    // print!("{} {}", s1, s2); // would fail, s1 invalid
    let s3 = s2.clone();
    println!("{} {}", s2, s3);

    let v = String::from("test");
    // makes "v" move into function, making it invalid here
    take_ownership(v);
    // println!("{}", v); // would crash, v is invalid due to function taking it

    let xx = 4;
    // xx would move into function, 
    // but i32 is Copy trait, 
    // so we can still use xx afterwards
    makes_copy(xx);

    // return values & scope
    let ss1 = gives_ownership();
    let ss2 = String::from("hello");
    let ss3 = takes_and_gives_back(ss2);
    println!("{} {}", ss1, ss3); // calling ss2 would crash, it moved
    // ss1 will be dropped (Drop trait) as its going out of scope
    // ss2 is moved into the takes_and_gives_back fn, so nothing happens
    // ss3 is dropped (Drop trait) as it was created via the returned value of takes_and_gives_back fn

    // "lending" values instead of giving ownership
    let sa1 = String::from("hello");
    let (sa2, len) = calculate_length(sa1);
    println!("The length of {} is {}", sa2, len);
    // but thats too much effort. hence the next chapter:

    // References & borrowing
    let aa1 = String::from("hello");
    let len = calculate_length_ref(&aa1);
    println!("The length of {} is {}", aa1, len);

    let ab1 = String::from("hello");
    // change_but_fail(&ab1); // can't change, as refs are not mutable by default
    let mut ab2 = String::from("hello!");
    change(&mut ab2);
    // this causes an issue: only one mutable ref at a time can be done

    // this fails:
    // let r1 = &mut ab2;
    // let r2 = &mut ab2;

    // we can have various scopes with mutable refs, just not simultaneous ones
    {
        let r1 = &mut ab2;
    }
    let r2 = &mut ab2;

    // having N immutable refs is OK, but if a mutable ref is created, crash
    // we dont want immutable refs to have stuff change without warning

    // but if immutable refs are no longer used, creating a mutable one is ok
    let mut zaa = String::from("test");
    let zaa1 = &zaa;
    let zaa2 = &zaa;
    println!("{} and {}", zaa1, zaa2);

    let zaa3 = &mut zaa;
    println!("{}", zaa3);

    let message = String::from("Welcome home, brave hunter");
    let welcome = &message[..7];
    let home = &message[8..12];

    println!("{} {} {}", welcome, home, first_word_with_slice(&message));
    // we could now optimize by using &str as input too
    let my_string = String::from("hello world");
    // 1st word works on slices of Strings
    let word = first_word_str(&my_string[..]);
    let my_string_literal = "hello world";
    // Because string leterals are string slices already
    // this works too without the slice syntax
    let word = first_word_str(&my_string_literal[..]);
    let word = first_word_str(my_string_literal);

    // there are also generic slices for other types
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
}

fn first_word_str(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

fn first_word_with_slice(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
fn first_word_pre_slice(s: &String) -> usize {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}

fn change(some_string: &mut String) {
    some_string.push_str("aaa");
}
// fn change_but_fail(some_string: &String) {
//     some_string.push_str("aaa");
// }
fn calculate_length_ref(s: &String) -> usize {
    s.len()
}
fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}
fn gives_ownership() -> String {
    let some_string = String::from("hello");
    some_string
    // BECUASE it is returned, the data will not be deleted. It will get assigned
    // to whatever it is getting assigned to, where the fn is called
}
fn takes_and_gives_back(a_string: String) -> String {
    a_string
    // we are now the owner of the a_string, and we give it to whoever
    // is getting it as assigned from where this method is called
}

fn take_ownership(somestr: String) {
    // BECAUSE its a STRING and has no Copy trait, it takes owner
    println!("{}", somestr);
    // somestr goes out of scope, drop called, memory freed
}
fn makes_copy(some_int: i32) {
    println!("{}", some_int);
    // some_int goes out of scope, nothing speecial happens, its a copy
}