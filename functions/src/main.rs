fn main() {
  println!("Hello, world!");
  another_function(124, 111);

  let x = 5;
  let y = {
    let x  = 3;
    x + 1 // no ; as its a return value
    // otherwise it becomes a statement, no return value
  };
  println!("Value of y is {}", y);
  println!("returningi32 returned {}", returningi32());
  println!("p_one returned {}", p_one(1));
}

fn another_function(x: i32, y: i32) {
  println!("Another function with value x{} and y{}", x, y);
}

fn returningi32() -> i32 {
  5
}
fn p_one(x: i32) -> i32 {
  x+1
}

fn a_function(x: i32, y: String) -> (i32, String) {
  return (10, String::from("Hello"))
}